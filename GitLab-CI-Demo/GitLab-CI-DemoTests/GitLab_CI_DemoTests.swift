//
//  GitLab_CI_DemoTests.swift
//  GitLab-CI-DemoTests
//
//  Created by Pradeep's Macbook on 28/04/21.
//  Copyright © 2021 Motiv Ate Fitness. All rights reserved.
//

import XCTest
@testable import GitLab_CI_Demo

class GitLab_CI_DemoTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let i = 0
        
        XCTAssertTrue(i==1, "I supposed to be zero")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
